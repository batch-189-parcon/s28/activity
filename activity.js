db.singleroom.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"roomsavailable": 10,
	"isAvailable": false
})






db.multiplerooms.insertMany([
		{

			"name": "double",
			"accomodates": 3,
			"price": 2000,
			"description": "A room fit for a small family going on a vacation",
			"roomsavailable": 5,
			"isAvailable": false

		},
		{
			"name": "queen",
			"accomodates": 4,
			"price": 4000,
			"description": "A room with a queen sized bed perfect for a simple getaway",
			"roomsavailable": 15,
			"isAvailable": false
		}



])






db.multiplerooms.find({
	"name": "double"
})




db.multiplerooms.updateOne(
	{
		"name": "queen"
	},
	{
		$set: {
			"roomsavailable": 0
		}
	}
)



db.users.deleteMany({
	"roomsavailable": 0
})